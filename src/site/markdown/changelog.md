# Changelog Samply Auth DTO

## Version 2.1 (NOT BACKWARDS COMPATIBLE)

- Added the Holder of Key JWT (HokToken) and the factory to create a new one
- Added the state parameter in the OAuth2 authentication
- Added the `token_type` and `expires_in` attributes in the access token DTO
- Added the maven site documentation

## Version 2.0

- Added the JWTVocabulary
- Added the Scope enum

## Version 1.8.1

- Added the `UserInfoDTO` class, used in the `/userinfo` resource

## Version 1.8.0

- Added the `usertype` attribute in the ID token
- Added the `roles` attribute in the ID token, which is a list
    of role names that user is member of
- Added the `permission` attribute in the Access token, which is
    a list of permissions

## Version 1.7.0

- Added the `type` attribute in all JWTs
