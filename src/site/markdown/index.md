# Samply Auth Data Transfer Objects

This library defines POJOs that must be used in the Samply Auth REST interface.
The Samply Auth identity provider is an OAuth2 implementation that extends the
OAuth2 specification by Samply specific requirements, see [RFC
6749](https://tools.ietf.org/html/rfc6749). Currently there are two servers
running with Samply Auth:

- https://auth.osse-register.de (productive system)
- https://auth.demo.osse-register.de (demo system)

Also this library offers JWT classes that can be used to verify signed access
tokens, ID tokens or refresh tokens. Each token has a different purpose:

- Use the access token in the HTTP `Authorization` header if your application
  uses the REST interface of another application. An access token is valid for
a short period of time. After this period of time you have to get a new access
token. Treat the access token like a password.
- Use the ID token to get the users real name, identity (unique ID), email
  address and other attributes.
- Use the refresh token to get a new access token


There are three ways to get an access token. It depends on the applications use
case which way should be used.

### 1. Your application is a registered client in the Samply Auth service

In this case you have a unique client ID (a random string that is public
knowledge), a client secret (only the Samply Auth service and your application
know this random string) and a static URL to your service. For this use case
you have to create a link on your web service that redirects the user to Samply
Auth with the following parameters:

- `client_id`: Your public client ID
- `redirect_uri`: The redirect URL. After the user authorizes your application
  to access his data, the user is redirected back to this URL.
- `scope`: a list of space separated scopes that your application requires

Use the `OAuth2ClientConfig.getRedirectUrl` method in this library to generate
the URL for the link. Once the user clicks on that link he will be asked to
login in Samply Auth. The application verifies that all data is valid (for
example that the redirect URL is valid for the client ID, user password
combination), generates a code for your application and redirects the user back
to the redirect URL that was specified in the link (with the code as
parameter). After this redirect it is your applications responsibility to use
the code, your client ID and your client secret to get a new access token.

See the REST interface description for more informations about this method.

### 2. Your application is *not* a registered client in the Samply Auth service

In this case your application acts on *one* users behalf (user `A`). Your
application has a private RSA key whose public key has been added in the Samply
Auth user interface. The application has the same permissions as the user `A`
that logs in with his username and password.

With your private key and the public keys ID your application can request a
code. Sign that code with your private key and send the signature along with
the code back to Samply Auth in order to get an access token. This method
requires the knowledge of:

- the public keys ID. Optional, use the SHA512 hash on your DER formatted
  public key if you do not have the ID
- the private key. Keep your private key secret.

Using this approach your application will only get an access token. Neither the
ID token nor the refresh token are returned in this approach, because neither
of those make sense in for this approach.

See the REST interface description for more informations about this method.


### 3. Your application already has an access token whose validity is nearing its end

An access token is valid for several hours. After this period of time, the
access token is no longer valid and should be exchanged for a new access token,
if necessary. You can use the refresh token that you got earlier to get a new
access token. This approach will only work if you used the first approach
earlier.

## Build

Use maven to build the jar:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>auth-dto</artifactId>
    <version>VERSION</version>
</dependency>
```
