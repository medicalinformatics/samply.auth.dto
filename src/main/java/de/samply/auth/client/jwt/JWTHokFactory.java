/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.auth.client.jwt;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;

import com.nimbusds.jose.JOSEException;

/**
 * JWT Holder of Key Factory. This factory generates a new HokToken using your private
 * key. Use this token in addition to an access token.
 */
public class JWTHokFactory {

    /**
     * Generates a new HokToken using your private key. Use this token in addition to an access token.
     * @param privateKey
     * @param publicKey
     * @param hours
     * @return
     * @throws JOSEException
     */
    public static String newHokToken(PrivateKey privateKey, PublicKey publicKey, int hours) throws JOSEException {
        JWT jwt = new JWT(JWTVocabulary.TokenType.HOK_TOKEN, 2);
        jwt.getBuilder().claim(JWTVocabulary.KEY, Base64.encodeBase64String(publicKey.getEncoded()));
        return jwt.sign(privateKey);
    }

    /**
     * Generates a new 256 bit strong random string
     *
     * @return a {@link java.lang.String} object.
     */
    public synchronized static String newRandomString() {
        return newRandomString(32);
    }

    /**
     * Generates a new random string with the given length.
     *
     * @return a {@link java.lang.String} object.
     */
    public synchronized static String newRandomString(int length) {
        return new BigInteger(length * 8, new SecureRandom()).toString(32);
    }

}
