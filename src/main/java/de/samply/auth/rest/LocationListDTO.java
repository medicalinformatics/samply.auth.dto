package de.samply.auth.rest;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement(name = "locations")
public class LocationListDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<LocationDTO> locations;

    /**
     * @return the locations
     */
    public List<LocationDTO> getLocations() {
        return locations;
    }

    /**
     * @param locations the locations to set
     */
    public void setLocations(List<LocationDTO> locations) {
        this.locations = locations;
    }

}
